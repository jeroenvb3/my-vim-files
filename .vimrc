let g:pathogen_disabled = ['syntastic', 'ale']
execute pathogen#infect()
syntax on
filetype plugin indent on
set number relativenumber
set statusline=%f%m%r%h%w\ [%l,%v][%p%%]\ [len=%L]
set laststatus=2
set shiftwidth=4
set tabstop=4
set expandtab

" Syntastic
 " set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
 " set statusline+=%*

 " let g:syntastic_mode_map = {
 "     \ "mode": "active",
 "     \ "passive_filetypes": ["javascript", "java"] }

map <Leader><S-e> :Explore<CR>/
map <Leader><S-v> :Vexplore!<CR>/
map <Leader><S-h> :Hexplore<CR>/
map <Leader>e <C-p>
map <Leader>v :vsplit<CR><C-W>l:CtrlP<CR>
map <Leader>h :split<CR><C-W>j:CtrlP<CR>

map <Leader>t :tabnew<CR>:CtrlP<CR>

"Explorer
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_list_hide= '.*\.args$,.*\.swp$,.*\.bbl$,.*\.aux$,.*\.blg$,.*\.out$,.*\.pdf$,.*\.toc$,.*\.log$,.*\.pyc$,venv'

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

set smartcase
set ignorecase
nnoremap <silent> <Leader>l ml:execute 'match Search /\%'.line('.').'l/'<CR>

" Set :W to also write file because this happens accidently too often.
command! W  write

" Comments
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1

" Git blame.
nnoremap <Leader>s :<C-u>call gitblame#echo()<CR>

set formatoptions-=o

map <Leader>zf zfi{

nnoremap <Leader>] <C-w>v<C-w>l<C-]>

set ff=unix

" NoMatchParen

autocmd BufNewFile,BufRead *.vue set syntax=html

" Copy pasting
vnoremap <C-c> "+y
map <C-p> "+P

set tags=$HOME/supertags

nnoremap <Leader>d :GitGutterToggle<CR>

" Settings for Ctrl-p
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_max_files=0
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
	\ --skip-vcs-ignores
	\ -g ""'

" FZF
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
map <Leader>f :Ag 

" Ripgrep
Plug 'mileszs/ack.vim'
" ack.vim --- {{{

" Use ripgrep for searching ⚡️
" Options include:
" --vimgrep -> Needed to parse the rg response properly for ack.vim
" --type-not sql -> Avoid huge sql file dumps as it slows down the search
" --smart-case -> Search case insensitive if all lowercase pattern, Search case sensitively otherwise
let g:ackprg = 'rg --vimgrep --type-not sql --smart-case'

" Auto close the Quickfix list after pressing '<enter>' on a list item
let g:ack_autoclose = 1

" Any empty ack search will search for the work the cursor is on
let g:ack_use_cword_for_empty_search = 1

" Don't jump to first match
cnoreabbrev Ack Ack!

" Maps <leader>/ so we're ready to type the search keyword
nnoremap <Leader>/ :Ack!<Space>
" }}}

" Navigate quickfix list with ease
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>
call plug#end()

" Tabs
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

map <Leader>g ma[["xy$`a:echo @x<CR>
