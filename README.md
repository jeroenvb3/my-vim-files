### My VIM config files.

These files are generally read from ~/.vim and ~/.vimrc.
```bash
ln -s $(pwd)/.vim $HOME/.vim

ln -s $(pwd)/.vimrc $HOME/.vimrc

ln -s $(pwd)/.agignore $HOME/.agignore

git submodule update --init
```

